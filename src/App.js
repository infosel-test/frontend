import React from 'react';
import Dashboard from './components/Dashboard'
import { Navbar } from 'react-bootstrap'
import logo from './images/logo.gif'

// Styles
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className="App">
      <Navbar>
        <Navbar.Brand href="#home"><img src={logo} alt="logo"/></Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            Logueado como: <a href="#login">dev</a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <Dashboard />
    </div>
  );
}

export default App;
