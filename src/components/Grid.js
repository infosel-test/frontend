import React, { useState } from 'react';
import ReactTable from 'react-table'
import axios from 'axios'

import { Row, Col, Button } from 'react-bootstrap'

import 'react-table/react-table.css'

const Grid = () => {
  // Datos iniciales
  const data = [
    {
      Tipo: 'Acción',
      Simbolo: "PE&OLES",
      UltimoHecho: 45.45,
      ValorAnterior: 45.82,
      VariacionUnitaria: -0.37,
      VariacionPorcentual: -0.807507,
      Fecha: "2019-07-23T00:00:00.000Z",
      Hora: "0956",
      Campo_001: "8762",
      Campo_008: "129"
    },
    {
      Tipo: 'Divisa',
      Simbolo: "MXNUS",
      PrecioCompra: 19.15,
      PrecioCompraAnterior: 19.10,
      VariacionUnitariaCompra: 0.05,
      VariacionPorcentualCompra: 0.261780000,
      PrecioVenta: 19.15,
      PrecioVentaAnterior: 19.10,
      VariacionUnitariaVenta: 0.05,
      VariacionPorcentualVenta: 0.261780000,
      Fecha: "2019-07-23T00:00:00.000Z",
      Hora: "1825",
      Campo_008: "44129"
    },
    {
      Tipo: 'Acción',
      Simbolo: "CEMEXCPO",
      UltimoHecho: 7.04,
      ValorAnterior: 7.05,
      VariacionUnitaria: 0.01,
      VariacionPorcentual: 0.142,
      Fecha: "2019-07-23T00:00:00.000Z",
      Hora: "1421",
      Campo_001: "334",
      Campo_008: "777"
    },
    {
      Tipo: 'Acción',
      Simbolo: "GMEXICOB",
      UltimoHecho: 43.55,
      ValorAnterior: 44.05,
      VariacionUnitaria: 0.50,
      VariacionPorcentual: 1.148,
      Fecha: "2019-07-23T00:00:00.000Z",
      Hora: "1425",
      Campo_001: "1133",
      Campo_008: "76253"
    },
    {
      Tipo: 'Divisa',
      Simbolo: "AUDUS",
      PrecioVenta: 0.6994,
      PrecioVentaAnterior: 0.7029,
      VariacionUnitariaVenta: -0.0035,
      VariacionPorcentualVenta: -0.4937,
      PrecioCompra: 0.6984,
      PrecioCompraAnterior: 0.7019,
      VariacionUnitariaCompra: -0.0035,
      VariacionPorcentualCompra: -0.641117,
      Fecha: "2019-07-23T00:00:00.000Z",
      Hora: "1825",
      Campo_008: "554263"
    }]

  const [instrumentos, setInstrumentos] = useState(data)
  
  function handleClick() {
    try {
      setInterval(() => {
        // Send data to backend & generate there the variation
      instrumentos.map(instrumento => {
        if (instrumento.UltimoHecho) {
          axios.post('http://localhost:3001/api/info', {
            clave: instrumento.UltimoHecho
          })
          .then(function (response) {
            // response.data.variation
            const variacion = Math.round(response.data.variation * 100) / 100
            instrumentos.map(i => {
              if(i.Simbolo === instrumento.Simbolo) {
                // La variación unitaria se calcula como una resta, es el Valor Actual menos el Valor Anterior
                i.VariacionUnitaria =  variacion - i.UltimoHecho
                i.VariacionUnitaria = Math.round(i.VariacionUnitaria * 100) / 100

                // Y la variación porcentual, es dividir la variación unitaria entre el valor anterior, Este resultado se multiplica por 100
                i.VariacionPorcentual = (i.VariacionUnitaria / i.UltimoHecho) * 100
                // i.VariacionPorcentual = Math.round(i.VariacionPorcentual * 100) / 100

                // Valor actual
                i.UltimoHecho = variacion
                
              }
              return ''
            })

            // update the data array
            setInstrumentos([...instrumentos])
          })
          .catch(function (error) {
            console.log(error)
          })
        }
        return ''
      })
    }, 1000)
  } catch(e) {
    console.log(e)
  }
  }

  const columns = [
    {
      Header: 'Tipo',
      accessor: 'Tipo',
      show: true
    },
    {
      Header: 'Clave',
      accessor: 'Simbolo' // String-based value accessors!
    }, 
    {
      Header: 'Ult Hecho',
      accessor: 'UltimoHecho'
    },
    {
      Header: 'Val Anterior',
      accessor: 'ValorAnterior'
    },
    {
      Header: 'Variación Unit',
      accessor: 'VariacionUnitaria'
    },
    {
      Header: 'Variación %',
      accessor: 'VariacionPorcentual'
    },
    {
      Header: '$ Venta',
      accessor: 'PrecioVenta'
    },
    {
      Header: '$ Anterior',
      accessor: 'PrecioVentaAnterior'
    },
    {
      Header: props => <span>Var UnitVenta</span>, // Custom header components!
      accessor: 'VariacionUnitariaVenta'
    },
    {
      Header: 'Var %Venta',
      accessor: 'VariacionPorcentualVenta'
    },
    {
      Header: '$Compra',
      accessor: 'PrecioCompra'
    },
    {
      Header: '$CompraAnterior',
      accessor: 'PrecioCompraAnterior'
    },
    {
      Header: 'VarUnitariaCompra',
      accessor: 'VariacionUnitariaCompra'
    },
    {
      Header: 'Var %Compra',
      accessor: 'VariacionPorcentualCompra' 
    },
    {
      Header: 'Fecha',
      accessor: 'Fecha'
    },
    {
      Header: 'Hora',
      accessor: 'Hora'
    },
    {
      Header: 'Campo_001',
      accessor: 'Campo_001'
    },
    {
      Header: 'Campo_008',
      accessor: 'Campo_008'
    }
  ]

  function getTRPropsType(state, rowInfo, column) {
    return {
      style: {
        background: rowInfo.row.VariacionUnitaria > 0 ? '#00CC00' : '#FF0000'
      }
    }
  }

  return (
    <>
    <Row className="pb-3">
      <Col>
        <Button onClick={handleClick} style={{backgroundColor: "#023669"}}>Iniciar Variación</Button>
      </Col>
    </Row>
      <ReactTable
        data={instrumentos}
        columns={columns}
        defaultPageSize={5}
        getTrProps={getTRPropsType}
        className="-striped -highlight"
        showPaginationBottom = {false}
      />
    </>
  )
}

export default Grid