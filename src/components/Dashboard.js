import React from 'react';
// import Admin from './Admin'
import Grid from './Grid'
import { Row, Col } from 'react-bootstrap'

const Dashboard = () => {
  return (
    <div className="container-fluid">
    <Row className="justify-content-md-center pt-2">
      <Col xs lg="6">
        <h1 style={{color: "#023669"}}>DASHBOARD - Acciones & Divisas</h1>
      </Col>
    </Row>
      {/* <Admin /> */}
      <Grid />
    </div>
  )
}

export default Dashboard