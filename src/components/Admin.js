import React, { Component } from 'react';
import { Row, Col, Accordion, Card, Button, Form } from 'react-bootstrap'

const columns = [
  {
    id: 0,
    short: 'type',
    long: 'Tipo'
  },
  {
    id: 1,
    short: 'ult-hecho',
    long: 'Último hecho'
  },
  {
    id: 2,
    short: 'val-ant',
    long: 'Valor Anterior'
  },
  {
    id: 3,
    short: 'var-unit',
    long: 'Variación Unitaria'
  },
  {
    id: 4,
    short: 'var-por',
    long: 'Variación Porcentual'
  },
  {
    id: 5,
    short: 'pre-ven',
    long: 'Precio de Venta'
  },
  {
    id: 6,
    short: 'pre-ant',
    long: 'Precio Anterior'
  }
]

class Admin extends Component {
  state = {
    type:  true,
    ulthecho: true,
    valant: true
  }

  handleClickCheckBox(box) {
    console.log(box.target.id + ' ' + box.target.checked)
    // this.setState({
    //   type: box.target.checked
    // })
  }

  render() {
    return (
      <Row>
        <Col>
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Admin
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Row>
                  <p>Columnas</p>
                    <Col>
                      <div key={`default-checkbox`} className="mb-3">
                        {columns.map(col => (
                          <Form.Check
                            type='checkbox'
                            key={`${col.id}-checkbox`} 
                            id={col.short}
                            label={col.long}
                            onChange={this.handleClickCheckBox}
                          />
                        ))}
                      </div>
                    
                    </Col>
                  </Row>
                </Card.Body>
                
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
      </Row>
    )
  }
}

export default Admin